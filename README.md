# Facture

une application, exécutable sur une JVM 1.8, qui imprime la facture détaillée

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

```
jdk 1.8

```

### Installing

clone the project in your local machine 


```
git clone https://gitlab.com/alaeddinehelmi/facturegenerator.git
```

install depnedency with the following command

```
mvn install
```

Run the generated jar file with:

```
java -jar calcultaxes/target/calcultaxes-0.0.1-SNAPSHOT.jar
```


## Running the tests

mvn test

## Authors

* **Ala eddine helmi haouala** - *Initial work* - (https://gitlab.com/alaeddinehelmi/facturegenerator)




