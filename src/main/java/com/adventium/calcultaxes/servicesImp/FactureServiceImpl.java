package com.adventium.calcultaxes.servicesImp;

import java.util.List;

import com.adventium.calcultaxes.model.Panier;
import com.adventium.calcultaxes.model.ProduitPanier;
import com.adventium.calcultaxes.services.FactureService;

public class FactureServiceImpl implements FactureService{

	public void generateFacture(List<Panier> panier) {
		System.out.println("###INPUT");

		for(int i=0;i<panier.size();i++) {
			System.out.println("####Input "+(i+1));
			List<ProduitPanier> produits= panier.get(i).getProduits();
			for(int j=0;j< produits.size() ;j++)
				System.out.println("* "+produits.get(j).getQuantite()+ " "+
				produits.get(j).getName()+" à "+produits.get(j).getPrix()
				+produits.get(j).getDevise());

		}

		System.out.println("###OUTPUT");

		for(int i=0;i<panier.size();i++) {
			System.out.println("####Output "+(i+1));
			List<ProduitPanier> produits= panier.get(i).getProduits();
			double totalTaxesParPanier = 0;
			double totalPrixParPanier = 0;
			for(int j=0;j<produits.size() ; j++) {
				System.out.println("* "+produits.get(j).getQuantite()+ " "+
						produits.get(j).getName()+" à "+produits.get(j).getPrix()
						+produits.get(j).getDevise()+" : "+caluculPrixProduitPanier(produits.get(j)));
				totalTaxesParPanier = totalTaxesParPanier + calculTaxesProduit(produits.get(j));
				totalPrixParPanier =  totalPrixParPanier + caluculPrixProduitPanier(produits.get(j));
			}
			totalPrixParPanier = totalPrixParPanier + totalTaxesParPanier;
			System.out.println("Montant des taxes :"+totalTaxesParPanier);
			System.out.println("Total : "+totalPrixParPanier);
		}
	}




	public double calculTaxesProduit(ProduitPanier produitPanier) {
		double taxe = 0;
		switch (produitPanier.getType().getId()) {
			case 2:
				taxe=produitPanier.getPrix()*0.1;
				break;
			case 3:
				taxe=produitPanier.getPrix()*0.2;
				break;
			case 1:
				taxe=produitPanier.getPrix()*0.0;
				break;
		}
		if(produitPanier.isImporte()) {
			taxe = taxe + produitPanier.getPrix()*0.05;
		}
		return (Math.round((taxe*produitPanier.getQuantite()+0.02)/0.05)*(0.05));


	}


	public double caluculPrixProduitPanier(ProduitPanier produit) {
		return Math.round((produit.getPrix())*20.0)/20.0*produit.getQuantite();
	}



}
