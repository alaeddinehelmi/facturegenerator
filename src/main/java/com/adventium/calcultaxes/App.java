package com.adventium.calcultaxes;

import java.util.ArrayList;
import java.util.List;

import com.adventium.calcultaxes.model.Panier;
import com.adventium.calcultaxes.model.ProduitPanier;
import com.adventium.calcultaxes.model.TypeProduit;
import com.adventium.calcultaxes.services.FactureService;
import com.adventium.calcultaxes.servicesImp.FactureServiceImpl;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args )
    {
    	TypeProduit typePremiernecessite = new TypeProduit(1 , "premiere necessité", 0);
    	TypeProduit typelivres = new TypeProduit(2 , "Livres", 10);
    	TypeProduit typeAutre= new TypeProduit(3 , "Autre produits", 20);

    	ProduitPanier produit1Panier1 = new ProduitPanier(1 , "Livres", 12.49, "€", typelivres, false, 2);
    	ProduitPanier produit2Panier1 = new ProduitPanier(2 , "CD musical", 14.99, "€", typeAutre, false, 1);
    	ProduitPanier produit3Panier1 = new ProduitPanier(3 , "barres de chocolat", 0.85, "€", typePremiernecessite, false, 3);

    	List<ProduitPanier> produits=new ArrayList<ProduitPanier>();

    	produits.add(produit1Panier1);
    	produits.add(produit2Panier1);
    	produits.add(produit3Panier1);

    	Panier panier1 = new Panier(produits);

    	ProduitPanier produit1Panier2 = new ProduitPanier(4 , "boîtes de chocolats importée", 10, "€", typePremiernecessite, true, 2);
    	ProduitPanier produit2Panier2 = new ProduitPanier(5 , "flacons de parfum importé", 47.50, "€", typeAutre, true, 3);

    	List<ProduitPanier> produits2=new ArrayList<ProduitPanier>();

    	produits2.add(produit1Panier2);
    	produits2.add(produit2Panier2);

    	Panier panier2 = new Panier(produits2);

    	ProduitPanier produit1Panier3 = new ProduitPanier(7 , "flacons de parfum importé", 27.99, "€", typeAutre, true, 2);
    	ProduitPanier produit2Panier3 = new ProduitPanier(8 , "flacon de parfum", 18.99, "€", typeAutre, false, 1);
    	ProduitPanier produit3Panier3 = new ProduitPanier(9 , "boîtes de pilules contre la migraine", 9.75, "€", typePremiernecessite, false, 3);
    	ProduitPanier produit4Panier3 = new ProduitPanier(10 , "boîtes de chocolats importés", 11.25, "€", typePremiernecessite, true, 2);

    	List<ProduitPanier> produits3=new ArrayList<ProduitPanier>();

    	produits3.add(produit1Panier3);
    	produits3.add(produit2Panier3);
    	produits3.add(produit3Panier3);
    	produits3.add(produit4Panier3);

    	Panier panier3 = new Panier(produits3);

    	List<Panier> paniers = new ArrayList<Panier>();
    	paniers.add(panier1);
    	paniers.add(panier2);
    	paniers.add(panier3);

    	FactureService factureService = new FactureServiceImpl();

    	factureService.generateFacture(paniers);


    }
}
