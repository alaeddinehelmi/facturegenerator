package com.adventium.calcultaxes.services;

import java.util.List;

import com.adventium.calcultaxes.model.Panier;
import com.adventium.calcultaxes.model.ProduitPanier;

public interface FactureService {

	public void generateFacture(List<Panier> panier);

	public double caluculPrixProduitPanier(ProduitPanier produit);

	public double calculTaxesProduit(ProduitPanier produitPanier);

}
