package com.adventium.calcultaxes.model;

public class TypeProduit {

	private int id;

	private String typeName;

	private double taxe;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public double getTaxe() {
		return taxe;
	}

	public void setTaxe(double taxe) {
		this.taxe = taxe;
	}



	public TypeProduit() {
		super();
	}

	public TypeProduit(int id, String typeName, int taxe) {
		super();
		this.id = id;
		this.typeName = typeName;
		this.taxe = taxe;
	}

	@Override
	public String toString() {
		return "TypeProduit [id=" + id + ", typeName=" + typeName + ", taxe=" + taxe + "]";
	}



}
