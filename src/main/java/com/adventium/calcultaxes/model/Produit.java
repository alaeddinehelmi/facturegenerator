package com.adventium.calcultaxes.model;

public class Produit {

	private long id;

	private String name;

	private double prix;

	private String devise;

	private TypeProduit type;

	private boolean importe;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getDevise() {
		return devise;
	}

	public void setDevise(String devise) {
		this.devise = devise;
	}

	public TypeProduit getType() {
		return type;
	}

	public void setType(TypeProduit type) {
		this.type = type;
	}

	public boolean isImporte() {
		return importe;
	}

	public void setImporte(boolean importe) {
		this.importe = importe;
	}

	public Produit(long id, String name, double prix, String devise, TypeProduit type, boolean importe) {
		super();
		this.id = id;
		this.name = name;
		this.prix = prix;
		this.devise = devise;
		this.type = type;
		this.importe = importe;
	}

	public Produit() {
		super();
	}

	@Override
	public String toString() {
		return "Produit [id=" + id + ", name=" + name + ", prix=" + prix + ", devise=" + devise + ", type=" + type
				+ ", importe=" + importe + "]";
	}


}
