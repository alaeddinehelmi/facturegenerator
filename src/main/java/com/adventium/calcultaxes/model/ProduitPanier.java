package com.adventium.calcultaxes.model;

public class ProduitPanier extends Produit{


	private int quantite;

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public ProduitPanier(long id, String name, double prix, String devise, TypeProduit type, boolean importe,
			int quantite) {
		super(id, name, prix, devise, type, importe);
		this.quantite = quantite;
	}


}
