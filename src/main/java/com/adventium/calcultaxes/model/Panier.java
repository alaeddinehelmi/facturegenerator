package com.adventium.calcultaxes.model;

import java.util.List;

public class Panier {



	private List<ProduitPanier> produits;

	public List<ProduitPanier> getProduits() {
		return produits;
	}

	public void setProduits(List<ProduitPanier> produits) {
		this.produits = produits;
	}



	public Panier() {
		super();
	}



	public Panier( List<ProduitPanier> produits) {
		super();
		this.produits = produits;
	}

	@Override
	public String toString() {
		return "Panier [produits=" + produits + "]";
	}




}
