package com.adventium.calcultaxes.factureServiceTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.adventium.calcultaxes.model.ProduitPanier;
import com.adventium.calcultaxes.model.TypeProduit;
import com.adventium.calcultaxes.services.FactureService;
import com.adventium.calcultaxes.servicesImp.FactureServiceImpl;

import junit.framework.TestCase;

public class FactureServiceTest extends TestCase {

	FactureService factureService;
	ProduitPanier produit1Panier1,produit2Panier1,produit3Panier1,produit4Panier1;
	@Before
	public void setUp() throws Exception {
		TypeProduit typePremiernecessite = new TypeProduit(1 , "premiere necessité", 0);
    	TypeProduit typelivres = new TypeProduit(2 , "Livres", 10);
    	TypeProduit typeAutre= new TypeProduit(3 , "Autre produits", 20);

    	 produit1Panier1 = new ProduitPanier(1 , "Livres", 12.49, "€", typelivres, false, 2);
    	 produit2Panier1 = new ProduitPanier(2 , "CD musical", 14.99, "€", typeAutre, false, 1);
    	 produit3Panier1 = new ProduitPanier(3 , "barres de chocolat", 0.85, "€", typePremiernecessite, false, 3);
    	 produit4Panier1 = new ProduitPanier(3 , "barres de chocolat importé", 0.85, "€", typePremiernecessite, true, 3);

    	factureService = new FactureServiceImpl();


	}

	@After
	public void tearDown() throws Exception {

	}


	@Test
	public void testCalculTaxesProduitTypeLivre() {
		double taxeProduit1 = factureService.calculTaxesProduit(produit1Panier1);
		assertEquals(2.5, taxeProduit1, 0.01);
	}

	@Test
	public void testCalculTaxesProduitTypeAutres() {
		double taxeProduit1 = factureService.calculTaxesProduit(produit2Panier1);
		assertEquals(3, taxeProduit1, 0.01);
	}


	@Test
	public void testCalculTaxesProduitTypeNecessite() {
		double taxeProduit1 = factureService.calculTaxesProduit(produit4Panier1);
		assertEquals(0.15, taxeProduit1, 0.01);
	}

	@Test
	public void testCalculTaxesProduitTypeNecessiteImportes() {
		double taxeProduit1 = factureService.calculTaxesProduit(produit3Panier1);
		assertEquals(0, taxeProduit1, 0);
	}
	@Test
	public void testCaluculPrixProduitPanier() {
		double prixProduit1 = factureService.caluculPrixProduitPanier(produit1Panier1);
		assertEquals(25, prixProduit1, 0);

	}


}
